#!/bin/sh

# sample
#./getLatestGitHubVerNo.sh VSCodium/vscodium
#./getLatestGitHubVerNo.sh ether/etherpad-lite


set -e
project=$1
characters=${2:-1-}
echo $(curl -s https://api.github.com/repos/${project}/releases/latest | jq --raw-output '.tag_name' | cut -c ${characters})
