FROM docker:latest

RUN apk update && apk upgrade \
    && apk add --no-cache python3 bash jq curl

ADD getLatestGitHubVerNo.sh /usr/local/bin/
